All commands are assumed to be used in root project folder

# Prerequisites

Prerequisites are the same as quickstart profile but with dependencies for secrets handling.

```
brew install helm
helm plugin install https://github.com/databus23/helm-diff
helm plugin install https://github.com/jkroepke/helm-secrets --version v3.12.0
brew install helmfile
brew install sops
```

# About full profile

Profile full is my personal installation and cannot be reused by another person since it uses encrypted secrets with my private key.
You have to create a new profile if you want to do your own installation. Folder profiles/full is mainly useful as a reference.

```
HOME_SERVER_PROFILE=full # Replace full by your own new profile
mkdir profiles/${HOME_SERVER_PROFILE}
```

# Secrets handling

Secrets can be present and committed as long as they are encrypted. It is recommended to use a KMS to do the encryption/decryption operations,
Google Cloud KMS is used here, but you can use another provider (see helm-secrets / helmfile / sops documentation and references.md).

## Creation of a private key to GCP Cloud KMS for encryption/decryption of secrets

```
gcloud auth login
gcloud auth application-default login
gcloud kms keyrings create sops --location global
gcloud kms keys create home-server-key --location global --keyring sops --purpose encryption
gcloud kms keys list --location global --keyring sops
```

## Encryption of global-secrets.yaml file

```
cp profiles/full/.sops.yaml profiles/${HOME_SERVER_PROFILE}/.sops.yaml 
# Replace gcp_kms with correct path to your key, then
touch profiles/${HOME_SERVER_PROFILE}/global-secrets.yaml 
# Fill this file with clear passwords in usual yaml format (don't commit the file to git for now!), then
helm secrets enc profiles/${HOME_SERVER_PROFILE}/global-secrets.yaml
# global-secrets.yaml secrets are now encrypted and file can be commited
```

Helmfile will automatically use Google Cloud KMS to decrypt the secrets during the `helmfile apply` command.

# Preparation of helmfile and config

Prepare global-values, helmfile and config following the full example (see also quickstart for a simpler example).

global-values.yaml is for values shared by several releases, prefixed by homeServer
global-secrets.yaml is for secrets shared by several releases, prefixed by homeServer

config/*/values.yaml.gotmpl is for the values of a specific release, it is a .gotmpl file so it can be processed
config/*/secrets.yaml.gotmpl is for the secrets of a specific release, it is a .gotmpl file so it can be processed

secrets.yaml files inside config/*/ can be encrypted following the same method as above

# Apply the helmfile

```
helmfile apply --state-values-set profile=${HOME_SERVER_PROFILE}
```