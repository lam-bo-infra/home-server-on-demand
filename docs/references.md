# Kubectl cheatsheet
https://kubernetes.io/docs/reference/kubectl/cheatsheet/

kubectl config get-contexts
kubectl config current-context                 
kubectl config use-context my-cluster-name

# Helmfile and helm secrets
https://lyz-code.github.io/blue-book/devops/helm/helm_secrets/
https://nikola.milojevic.me/blog/helm-secrets-skaffold-gcp/

For secrets sealed-secrets is also an alternative (secrets are decrypted server side instead of client side)

# Cloud solutions with no CB free tiers

Kafka - Upstash 

Database - CockroackDB