
# About

Quickstart installation install the minimal for a simple demo
 - keda (with Http Addon) WIP
 - simple Whoami application with Http autoscaling WIP

# Prerequisites

brew install helm
helm plugin install https://github.com/databus23/helm-diff
brew install helmfile

# Install the quickstart or upgrade it

helmfile apply --state-values-set profile=quickstart

# Remove the releases

helm uninstall quickstart-whoami -n quickstart-whoami