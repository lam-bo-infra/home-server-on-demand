# About hibernateDialect
```
externalDatabase.hibernateDialect = org.hibernate.dialect.CockroachDB201Dialect
```
is only necessary if CockroachDB is used
See https://stackoverflow.com/questions/64151715/spring-boot-vs-postgres-vs-cockroach-the-fastpath-function-lo-creat-is-unknow


# Using an k8s secret instead of providing the database password

See https://dataflow.spring.io/docs/installation/kubernetes/helm/#database-parameters

- externalDatabase.existingPasswordSecret is an existing secret with database password
- externalDatabase.existingPasswordKey is the key of the existing secret with database password, 
  defaults to datasource-password (password itself must be base64 encoded).
  
