
# Home-server-on-demand (WIP)

This project is a helm chart museum for my personal home server and also an example of a "serverless" k8s cluster:
 - A lot of applications are deployed on this cluster.
 - Replicas of most deployments are set to 0 by default, they only go to 1 when they are waked by a http request 
   and then go back to sleep, application startup is slow but resources consumption should be low. 
   
It is not production ready because it's based on Keda http add-on which is in beta, but it can be useful in some personal projects:
 - You want to deploy a lot of applications, but you use them only one at a time like emby, code-server... 
   Then you can use a small k8s cluster (my personal home is just a k3s cluster with only one node, an 8 gb memory Intel mini-pc).
 - You want to deploy to a cloud provider and minimize the operating costs 
   (GKE Autopilot is a good fit for this since you pay per pods instead of provisioned vms).
   
Several installation profiles are available depending on your interests, you can check:
 - [docs/quickstart.md](docs/quickstart.md) for a basic installation with just keda-http and a whoami application.
 - [docs/full.md](docs/full.md) for a full step-by-step guide to deploy your own home server.

See [docs/references.md](docs/references.md) for useful links and commands.